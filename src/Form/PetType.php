<?php

namespace App\Form;

use App\Entity\Pet;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;



class PetType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

        ->add('category',ChoiceType::class, [
            'choices'  => [
                'Cat' => 1,
                'Dog' => 2,
                'Fish' => 3,
                'Birds' => 4,
                'Reptiles' => 5,
            ]])
        ->add('name',TextType::class,
            [
                'required' => true, 
                'attr' => ['placeholder' => 'Tom']
            ]
        )
        ->add('photoUrls', FileType::class,[
            'label' => 'Select image (png,jpg,jpeg file)',
            'mapped' => false,
            'required' => false
        ])
        ->add('tags',TextType::class,[
            'required' => true,
            'attr' => ['placeholder'=>'tag1,tag2,tag3']
        ])
        ->add('status',ChoiceType::class,[
            'choices' => [
                'available'=>1,
                'no available' =>0
            ]])

        ->add('save', SubmitType::class,
                    ['label' => 'Save']);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Pet::class,
        ]);
    }
}
