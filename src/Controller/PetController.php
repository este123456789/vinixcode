<?php

namespace App\Controller;

use App\Entity\Pet;
use App\Form\PetType;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class PetController extends AbstractController
{




    /**
     * @Route("/pet", name="pet")
     */
    public function index(): Response
    {

    	$pets = $this->getDoctrine()->getRepository('App\Entity\Pet')->findAll();
      $data = [
        'pets' => $pets
      ];


      $response = array();
      foreach ($pets as $pet) {
        $response[] = array(
          'pet_id' => $pet->getId(),
          'name' => $pet->getName(),
          'photoUrl' => $pet->getPhotoUrls(),
          'tags' => $pet->getTags(),
          'status' => $pet->getStatus()
        );
      }


      return new JsonResponse($response);
    }


    /**
     * @Route("/findbystatus", name="findbystatus")
     */
    public function findbystatus(Request $request,$id): Response
    {

      $em = $this->getDoctrine()->getManager();
      $pet = $em->getRepository('App\Entity\Pet')->findBy(array('id'=> $id));

      if (!$pet) {

        throw $this->createNotFoundException(
          'There are no pets with the following id: ' . $id
        );
      }



      $response = array();
      foreach ($pet as $pt) {
        $response[] = array(
          'pet_id' => $pt->getId(),
          'name' => $pt->getName(),
          'photoUrl' => $pt->getPhotoUrls(),
          'tags' => $pt->getTags(),
          'status' => $pt->getStatus()
        );
      }


      return new JsonResponse($response);

    }



    /**
     * @Route("/view", name="view")
     */
    public function view(Request $request,$id): Response
    {



      $pets = $this->getDoctrine()->getRepository('App\Entity\Pet')->findAll();
      $data = [
        'pets' => $pets
      ];




      $em = $this->getDoctrine()->getManager();
      $pet = $em->getRepository('App\Entity\Pet')->findBy(array('id'=> $id));

      if (!$pet) {

        throw $this->createNotFoundException(
          'There are no pets with the following id: ' . $_GET['id']
        );
      }



      $response = array();
      foreach ($pet as $pt) {
        $response[] = array(
          'pet_id' => $pt->getId(),
          'name' => $pt->getName(),
          'photoUrl' => $pt->getPhotoUrls(),
          'tags' => $pt->getTags(),
          'status' => $pt->getStatus()
        );
      }


      return new JsonResponse($response);

    }



    /**
     * @Route("/edit", name="edit")
     */
    public function edit(Request $request,$id): Response
    {


      $em = $this->getDoctrine()->getManager();
      $pet = $em->getRepository('App\Entity\Pet')->findBy(array('id'=> $id));

      if (!$pet) {

        throw $this->createNotFoundException(
          'There are no pets with the following id: ' . $id
        );
      }


      $form = $this->createForm(PetType::class, $pet);
      $form->handleRequest($request);

      if ($form->isSubmitted() && $form->isValid() ) {
        $pet = $form->getData();
        $dir_subida =  $this->getParameter('kernel.project_dir') . '/public/img/';
        $fichero_subido = $dir_subida.$_FILES["pet"]["name"]['photoUrls'];
        $em = $this->getDoctrine()->getManager();

        if(!empty($_FILES["pet"]["name"]['photoUrls'])){

          move_uploaded_file($_FILES["pet"]["tmp_name"]['photoUrls'], $fichero_subido);
          $pet->setPhotoUrls($_FILES["pet"]["name"]['photoUrls']);
        }    
        $em->persist($pet);
        $em->flush();

      }

      $response = array();
      foreach ($pet as $pt) {
        $response[] = array(
          'pet_id' => $pt->getId(),
          'name' => $pt->getName(),
          'photoUrl' => $pt->getPhotoUrls(),
          'tags' => $pt->getTags(),
          'status' => $pt->getStatus()
        );
      }


      return new JsonResponse($response);

    }


    /**
     * @Route("/add", name="add")
     */
    public function add(Request $request,$id): Response {

      $em = $this->getDoctrine()->getManager();
      $pet = $em->getRepository('App\Entity\Pet')->findBy(array('id'=> $id));

      if ($pet) {

        throw $this->createNotFoundException(
          'There are no pets with the following id: ' . $id
        );
      }
      else {


        $form = $this->createForm(PetType::class, $pet);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid() ) {
          $pet = $form->getData();
          $dir_subida =  $this->getParameter('kernel.project_dir') . '/public/img/';
          $fichero_subido = $dir_subida.$_FILES["pet"]["name"]['photoUrls'];
          $em = $this->getDoctrine()->getManager();

          if(!empty($_FILES["pet"]["name"]['photoUrls'])){

            move_uploaded_file($_FILES["pet"]["tmp_name"]['photoUrls'], $fichero_subido);
            $pet->setPhotoUrls($_FILES["pet"]["name"]['photoUrls']);
          }    
          $em->persist($pet);
          $em->flush();

        }

        $response = array();
        foreach ($pet as $pt) {
          $response[] = array(
            'pet_id' => $pt->getId(),
            'name' => $pt->getName(),
            'photoUrl' => $pt->getPhotoUrls(),
            'tags' => $pt->getTags(),
            'status' => $pt->getStatus()
          );
        }


        return new JsonResponse($response);

      }


    }





    /**
     * @Route("/find", name="find")
     */
    public function find(Request $request,$id): Response {

      $em = $this->getDoctrine()->getManager();
      $pet = $em->getRepository('App\Entity\Pet')->findBy(array('status'=> $id));


      if (!$pet) {

        throw $this->createNotFoundException(
          'There are no pets with the following id: ' . $id
        );
      }
      

      
        $response = array();
        foreach ($pet as $pt) {
          $response[] = array(
            'pet_id' => $pt->getId(),
            'name' => $pt->getName(),
            'photoUrl' => $pt->getPhotoUrls(),
            'tags' => $pt->getTags(),
            'status' => $pt->getStatus()
          );
        }


        return new JsonResponse($response);

      


    }












  }
