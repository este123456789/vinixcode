<?php

namespace App\Controller;


use App\Entity\Pet;
use App\Form\PetType;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
class HomeController extends AbstractController
{
    /**
     * @Route("/home", name="home")
     */
    public function index(Request $request): Response
    {
        $pets = $this->getDoctrine()->getRepository('App\Entity\Pet')->findAll();
        $pet = new Pet();
        $form = $this->createForm(PetType::class, $pet);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid() ) {
        	$pet = $form->getData();
            $em = $this->getDoctrine()->getManager();


            if(isset($_FILES["pet"]["name"]['photoUrls']) && !empty($_FILES["pet"]["name"]['photoUrls'])){

                $dir_subida =  $this->getParameter('kernel.project_dir') . '/public/img/';
                $fichero_subido = $dir_subida.$_FILES["pet"]["name"]['photoUrls'];
                move_uploaded_file($_FILES["pet"]["tmp_name"]['photoUrls'], $fichero_subido);
                $pet->setPhotoUrls($_FILES["pet"]["name"]['photoUrls']);
            }
            $em->persist($pet);
            $em->flush();
            return $this->redirect('/home');

        }


        return $this->render('home/index.html.twig', [
            'controller_name' => 'HomeController',
            'form' => $form->createView(),
            'pets'=> $pets
        ]);
    }




    /**
     * @Route("/updatepet", name="updatepet")
     */
    public function updatepet(Request $request): Response
    {
        $pets = $this->getDoctrine()->getRepository('App\Entity\Pet')->findAll();
        $em = $this->getDoctrine()->getManager();
        $pet = $em->getRepository('App\Entity\Pet')->find($_GET['id']);

        if(isset($_GET['del'])  && $_GET['del'] == 'true'){
            $em = $this->getDoctrine()->getManager();
            $pet = $em->getRepository('App\Entity\Pet')->find($_GET['id']);

            if (!$pet) {
                throw $this->createNotFoundException(
                    'There are no pets with the following id: ' . $_GET['id']
                );
            }

            $em->remove($pet);
            $em->flush();
            return $this->redirect('/home');

        }

        if (!$pet) {
            throw $this->createNotFoundException(
                'There are no pets with the following id: ' . $id
            );
        }

        $form = $this->createForm(PetType::class, $pet);
        $form->handleRequest($request);

        if(isset($_GET['update'])  && $_GET['update'] == 'true'){



            if ($form->isSubmitted() && $form->isValid() ) {
                $pet = $form->getData();
                $dir_subida =  $this->getParameter('kernel.project_dir') . '/public/img/';
                $fichero_subido = $dir_subida.$_FILES["pet"]["name"]['photoUrls'];
                $em = $this->getDoctrine()->getManager();

                if(!empty($_FILES["pet"]["name"]['photoUrls'])){

                    move_uploaded_file($_FILES["pet"]["tmp_name"]['photoUrls'], $fichero_subido);
                    $pet->setPhotoUrls($_FILES["pet"]["name"]['photoUrls']);
                }    
                $em->persist($pet);
                $em->flush();
                return $this->redirect('/home');
            }


        }

        return $this->render(
            'home/index.html.twig',
            array(
                'form' => $form->createView(),
                'pet' => $pet,
                'pets' => $pets
            )
        ); 



}

}
