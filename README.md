# App / Pets /

_Aplicacion realizada para vinixcode por Esteban Villa R. 2021_

## Instalación 🚀
«
_1. Instale xampp https://www.apachefriends.org/es/index.html o similares_
»
«
###
_2. Instale git shell https://gitforwindows.org/_
»
«
###
_3. Instale la base de datos vinixcodeappdb.sql _
»
«
###
_4. Instale composer https://getcomposer.org/download/ _
»
«
###
_5. Clone el  repositorio con el comando git@gitlab.com:este123456789/vinixcode.git  en una carpeta del servidor, si eligio xampp seria en la carpeta htdocs_
»
«
###
_6. En la carpeta raiz del sitio ejecute el comando composer install _
»
«
###
_7. Ejecute el comando  symfony server:start para iniciar el programa_
»
«
###
_8. Urls Frontend Funcional implementando servicios: _
»
«
###
_ INICIO _
»
«
###
_ http://127.0.0.1:8000/home _ 
«
»_ Url Backend servicios: _
»
«
###»
«
###
_ Buscar por id : http://127.0.0.1:8000/pet/30_
»
«
###
_ Buscar por estado: http://127.0.0.1:8000/pet/findbystatus/1_
»
«
###
_ Agregar nuevo registro: http://127.0.0.1:8000/pet/add/33 _
»
«
###
_ Editar registro: http://127.0.0.1:8000/pet/edit/31_
»
«
###




### Pre-requisitos 📋
»
«
###
_  xampp https://www.apachefriends.org/es/index.html _
»
«
###
_  git shell https://gitforwindows.org/ _
»
«
###
_ composer https://getcomposer.org/download/ _
»
«
###
## Versionado 📌
»
«
###
1.0.0
»
«
###
## Autor ✒️
»
«
###
Esteban Villa Ramirez. 2021
»